<?php

namespace jj\Ttbot\Command;

use jj\Ttbot\Crawler;
use jj\Ttbot\Env;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TelegramBot\Api\BotApi;

/**
 * Class CrawlerCommand
 *
 * @package jj\Ttbot\Command
 */
class CrawlerCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'ttbot:crawler';

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        while (true) {
            foreach (Env::get(Env::TELEGRAM_TOKENS) as $telegramToken) {
                $bot = new BotApi($telegramToken);
                foreach (Env::get(Env::SITES) as $site) {
                    $crawler = new Crawler($site);
                    foreach ($crawler->getNews() as $news) {
                        if ($this->isNew($news)) {
                            foreach (Env::get(Env::TELEGRAM_CHAT_IDS) as $chatId) {
                                $bot->sendMessage($chatId, $news['text'] . \PHP_EOL . $news['link']);
                            }
                        }
                    }
                }
            }
            $output->writeln((new \DateTime)->format('Y-m-d H.i:s'));
            sleep(600);
        }

        return 0;
    }

    /**
     * @param $news
     *
     * @return bool
     */
    private function isNew($news): bool
    {
        $tmpFile = \dirname(__DIR__, 4) . \DIRECTORY_SEPARATOR . 'var' . \DIRECTORY_SEPARATOR . 'crwaler';

        $news = $news['text'] . ' - ' . $news['link'];


        $isNew = false;
        if (\strpos(\is_file($tmpFile) ? \file_get_contents($tmpFile) : '', $news) === false) {
            $isNew = true;
            $h = \fopen($tmpFile, 'a');
            \fwrite($h, $news . \PHP_EOL);
            \fclose($h);
        }

        return $isNew;
    }
}