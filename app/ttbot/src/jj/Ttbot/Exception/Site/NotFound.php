<?php

namespace jj\Ttbot\Exception\Site;

use jj\Ttbot\Exception\Site;

/**
 * Class NotFound
 *
 * @package jj\Ttbot\Exception
 */
class NotFound extends Site
{
}