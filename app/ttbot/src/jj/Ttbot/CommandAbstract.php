<?php

namespace jj\Ttbot;

use Symfony\Component\Console\Command\Command;

/**
 * Class CommandAbstract
 *
 * @package gcs\Chatbot
 */
abstract class CommandAbstract extends Command
{
    /**
     * @return array
     * @throws Exception
     */
    public static function getAllCommands()
    {
        return static::findCommandsInPath(__DIR__ . \DIRECTORY_SEPARATOR . 'Command');
    }

    /**
     * @param $path
     *
     * @return array
     * @throws Exception
     */
    protected static function findCommandsInPath($path): array
    {
        $commands = [];
        foreach (\glob($path . \DIRECTORY_SEPARATOR . '*') as $file) {
            if (\is_dir($file)) {
                foreach (static::{__FUNCTION__}($file) as $command) {
                    $commands[] = $command;
                }
            } elseif (\substr($file, -11) === 'Command.php') {
                $commandClass = '\\' . __NAMESPACE__ . \str_replace(\DIRECTORY_SEPARATOR, '\\', \substr($file, \strlen(__DIR__), -4));

                if (!\class_exists($commandClass)) {
                    throw new Exception('Command class "' . $commandClass . '" could not find, use file "' . $file . '"');
                }

                $commands[] = new $commandClass;
            }
        }

        return $commands;
    }
}