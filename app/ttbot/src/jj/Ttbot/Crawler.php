<?php

namespace jj\Ttbot;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use jj\Ttbot\Exception\Site\NotFound;

class Crawler
{
    /**
     * @var @string
     */
    private $site;

    public function __construct(string $site)
    {
        $this->setSite($site);
    }

    /**
     * @return mixed
     */
    private function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     *
     * @return Crawler
     */
    private function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return array
     * @throws NotFound
     */
    public function getNews(): array
    {
        try {
            // Guzzle und curl liefert einfach immer nichts zurück. deswegen der Umweg über wget
            $tmp = \sys_get_temp_dir() . \DIRECTORY_SEPARATOR . \microtime(true);
            \touch($tmp);
            \exec('wget ' . $this->getSite() . ' -q -O ' . $tmp);
            $content = \file_get_contents($tmp);
            \unlink($tmp);

            $start = \strpos($content, '<div class="container" id="content-container">');
            $content = \substr($content, $start);

            $start = \strpos($content, '<h1>');
            $end = \strpos($content, '</h1>');
            $headline = \substr($content, $start + 4, $end - $start);
            $end = \strpos($headline, '<');
            $headline = \trim(\substr($headline, 0, $end));

            $start = \strpos($content, 'id="playingPlanDesktop"');
            $content = \substr($content, $start);
            $start = \strpos($content, '<tbody>');
            $content = \substr($content, $start);
            $end = \strpos($content, '</table>');
            $content = \substr($content, 0, $end);

            $games = \explode('<tr>', $content);

            \array_shift($games);

            $results = [];
            foreach ($games as $game) {
                $gameData = explode('<td', $game);

                $link = $gameData[7];
                $start = \strpos($link, '<a href="');
                $link = \substr($link, $start + 9);
                $end = \strpos($link, '">');
                $link = \substr($link, 0, $end);
                if ($link) {
                    $link = 'https://www.mytischtennis.de' . $link;
                }

                foreach ($gameData as $k => $v) {
                    $start = \strpos($v, '>');
                    $gameData[$k] = \trim(\strip_tags(\substr($v, $start + 1)));
                }

                if ($gameData[1]) {
                    $datum = $gameData[1];
                }
                $uhrzeit = \substr($gameData[2], 0, 5);

                $heim = $gameData[4];
                $gast = $gameData[5];
                $ergebnis = \substr($gameData[7], 0, 3);

                if ($ergebnis && $ergebnis !== '0:0') {
                    $results[] = [
                        'text' => $headline . ' - ' . $datum . ' ' . $uhrzeit . ' - ' . $heim . ' - ' . $gast . ': ' . $ergebnis,
                        'link' => $link,
                    ];
                }
            }

            return $results;
        } catch (RequestException $e) {
            if ($e->getCode() === 404) {
                var_dump($e->getMessage());
                throw new NotFound($this->getSite());
            }
            throw  $e;
        }
    }
}