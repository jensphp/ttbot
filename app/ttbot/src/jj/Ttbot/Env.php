<?php

namespace jj\Ttbot;

/**
 * Class Env
 *
 * @package jj\Ttbot
 */
class Env
{
    /**
     * @const string
     */
    public const SITES = 'TTBOT_SITES';
    /**
     * @const string
     */
    public const TELEGRAM_TOKENS = 'TTBOT_TELEGRAM_TOKENS';
    /**
     * @const string
     */
    public const TELEGRAM_CHAT_IDS = 'TTBOT_TELEGRAM_CHAT_IDS';
    /**
     * @const string
     */
    private const ARRAY_SEPERATOR = '|';

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key)
    {
        $value = \getenv($key);

        if (\substr($value, 0, 1) === '[' && \substr($value, -1) === ']') {
            $value = \substr($value, 1, -1);
            $value = \explode(self::ARRAY_SEPERATOR, $value);
        }

        return $value;
    }
}